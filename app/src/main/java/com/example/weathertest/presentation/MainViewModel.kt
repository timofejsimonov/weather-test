package com.example.weathertest.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.example.weathertest.domain.useCase.GetWeatherUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

class MainViewModel(private val getWeatherUseCase: GetWeatherUseCase) : ViewModel() {

    private val _screenState: MutableStateFlow<MainScreenState> =
        MutableStateFlow(MainScreenState.Init)
    val screenState: StateFlow<MainScreenState>
        get() = _screenState.asStateFlow()

    init {
        loadForecast("tomsk", 5)
    }

    private fun loadForecast(city: String, period: Int) {
        _screenState.value = MainScreenState.Loading

        viewModelScope.launch(Dispatchers.IO) {
            try {
                _screenState.value = MainScreenState.Data(getWeatherUseCase(city, period))
            } catch (t: Throwable) {
                _screenState.value = MainScreenState.Error
            }
        }
    }

    class Factory @Inject constructor(
        private val getWeatherUseCase: GetWeatherUseCase
    ) :
        ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            @Suppress("UNCHECKED_CAST")
            return MainViewModel(getWeatherUseCase) as T
        }
    }
}