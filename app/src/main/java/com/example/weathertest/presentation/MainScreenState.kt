package com.example.weathertest.presentation

import com.example.weathertest.domain.entity.DayEntity

sealed interface MainScreenState {
    object Init : MainScreenState
    object Loading : MainScreenState
    class Data(val list: List<DayEntity>) : MainScreenState
    object Error : MainScreenState
}