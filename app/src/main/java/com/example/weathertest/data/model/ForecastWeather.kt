package com.example.weathertest.data.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class ForecastWeather(
    @SerialName("forecastday")
    val forecastDay: List<Day>
)
