package com.example.weathertest.data.repository

import android.util.Log
import com.example.weathertest.data.model.WeatherResponse
import com.example.weathertest.data.retrofit.WeatherService
import com.example.weathertest.domain.entity.DayEntity
import com.example.weathertest.domain.repository.WeatherRepository
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

class WeatherRepositoryImpl @Inject constructor(private val weatherService: WeatherService) :
    WeatherRepository {

    override suspend fun getWeather(city: String, period: Int): List<DayEntity> =
        suspendCoroutine { continuation ->
            weatherService.getForecast(city, period).enqueue(
                object : Callback<WeatherResponse> {

                    override fun onResponse(
                        call: Call<WeatherResponse>,
                        response: Response<WeatherResponse>
                    ) {
                        if (response.code() != CONDITIONS_ERROR_CODE)
                            continuation.resume((response.body() as WeatherResponse).toEntityList())
                    }

                    override fun onFailure(call: Call<WeatherResponse>, t: Throwable) {
                        continuation.resumeWithException(t)
                    }
                })
        }

    companion object {
        const val CONDITIONS_ERROR_CODE = 400
    }
}