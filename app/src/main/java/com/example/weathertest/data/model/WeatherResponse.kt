package com.example.weathertest.data.model

import kotlinx.serialization.Serializable

@Serializable
data class WeatherResponse(
    val forecast: ForecastWeather
) {
    fun toEntityList() =
        forecast.forecastDay.map { it.toEntity() }
}
