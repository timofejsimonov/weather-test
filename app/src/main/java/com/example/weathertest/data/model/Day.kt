package com.example.weathertest.data.model

import com.example.weathertest.domain.entity.DayEntity
import kotlinx.serialization.Serializable

@Serializable
data class Day(
    val date: String,
    val day: DayWeather
) {
    fun toEntity() =
        DayEntity(
            date,
            day.condition.text,
            day.condition.icon,
            day.avgTempC,
            day.maxWindKph,
            day.avgHumidity
        )
}
