package com.example.weathertest.data.retrofit

import com.example.weathertest.data.model.WeatherResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherService {
    @GET("forecast.json")
    fun getForecast(
        @Query("q") city: String,
        @Query("days") period: Int,
        @Query("key") key: String = KEY
    ) : Call<WeatherResponse>

    companion object {
        private const val KEY = "c9444bc6ba11406aa23135904231708"
    }
}