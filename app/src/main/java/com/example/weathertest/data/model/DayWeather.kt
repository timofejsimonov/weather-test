package com.example.weathertest.data.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class DayWeather(
    @SerialName("avgtemp_c")
    val avgTempC: Float,
    @SerialName("maxwind_kph")
    val maxWindKph: Float,
    @SerialName("avghumidity")
    val avgHumidity: Float,
    val condition: Condition
)
