package com.example.weathertest.domain.repository

import com.example.weathertest.domain.entity.DayEntity

interface WeatherRepository {
    suspend fun getWeather(city: String, period: Int): List<DayEntity>
}