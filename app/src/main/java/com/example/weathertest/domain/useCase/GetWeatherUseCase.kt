package com.example.weathertest.domain.useCase

import com.example.weathertest.domain.repository.WeatherRepository
import javax.inject.Inject

class GetWeatherUseCase @Inject constructor(private val repository: WeatherRepository) {
    suspend operator fun invoke(city: String, period: Int) =
        repository.getWeather(city, period)
}