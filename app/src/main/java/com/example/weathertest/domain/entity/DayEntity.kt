package com.example.weathertest.domain.entity

data class DayEntity(
    val date: String,
    val conditionText: String,
    val conditionIcon: String,
    val avgTempC: Float,
    val maxWindKph: Float,
    val avgHumidity: Float
)
