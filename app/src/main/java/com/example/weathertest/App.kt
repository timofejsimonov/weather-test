package com.example.weathertest

import android.app.Application
import com.example.weathertest.di.AppComponent
import com.example.weathertest.di.DaggerAppComponent

class App : Application() {
    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()

        appComponent = DaggerAppComponent.factory().create()
    }
}