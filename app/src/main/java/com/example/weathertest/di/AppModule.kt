package com.example.weathertest.di

import com.example.weathertest.data.repository.WeatherRepositoryImpl
import com.example.weathertest.data.retrofit.WeatherService
import com.example.weathertest.domain.repository.WeatherRepository
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import dagger.Module
import dagger.Provides
import dagger.Reusable
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import okhttp3.MediaType
import retrofit2.Retrofit

@Module
class AppModule {

    @Reusable
    @Provides
    fun provideJson(): Json = Json {
        ignoreUnknownKeys = true
    }

    @OptIn(ExperimentalSerializationApi::class)
    @Reusable
    @Provides
    fun provideRetrofitClient(json: Json): Retrofit =
        Retrofit.Builder()
            .baseUrl(URL)
            .addConverterFactory(json.asConverterFactory(MediaType.get("application/json")))
            .build()

    @Reusable
    @Provides
    fun provideChannelsService(retrofit: Retrofit): WeatherService =
        retrofit.create(WeatherService::class.java)

    @Reusable
    @Provides
    fun provideWeatherRepository(impl: WeatherRepositoryImpl): WeatherRepository = impl

    companion object {
        private const val URL = "https://api.weatherapi.com/v1/"
    }
}