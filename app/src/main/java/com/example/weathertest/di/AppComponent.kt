package com.example.weathertest.di

import android.content.Context
import com.example.weathertest.App
import com.example.weathertest.MainActivity
import com.example.weathertest.ui.main.MainFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {
    fun inject(activity: MainActivity)
    fun inject(fragment: MainFragment)

    @Component.Factory
    interface Factory {
        fun create(): AppComponent
    }
}

fun Context.getAppComponent(): AppComponent = (this.applicationContext as App).appComponent