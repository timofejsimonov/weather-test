package com.example.weathertest.ui.main

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.weathertest.R
import com.example.weathertest.databinding.FragmentMainBinding
import com.example.weathertest.di.getAppComponent
import com.example.weathertest.domain.entity.DayEntity
import com.example.weathertest.presentation.MainScreenState
import com.example.weathertest.presentation.MainViewModel
import com.example.weathertest.ui.autoCleared
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

class MainFragment : Fragment() {

    @Inject
    lateinit var vmFactory: MainViewModel.Factory
    private val viewModel: MainViewModel by viewModels { vmFactory }

    private var binding: FragmentMainBinding by autoCleared()

    override fun onAttach(context: Context) {
        super.onAttach(context)
        requireContext().getAppComponent().inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMainBinding.inflate(inflater, container, false)

        viewModel.screenState
            .flowWithLifecycle(lifecycle)
            .onEach { render(it) }
            .launchIn(lifecycleScope)

        return binding.root
    }

    private fun render(state: MainScreenState) {
        when (state) {
            is MainScreenState.Loading -> {
                binding.progressBar.isVisible = true
            }
            is MainScreenState.Data -> {
                binding.progressBar.isVisible = false
                setupAdapter(state.list)
            }
            is MainScreenState.Error -> {
                binding.progressBar.isVisible = false
                Toast.makeText(requireContext(), getString(R.string.error), Toast.LENGTH_LONG).show()
            }
            is MainScreenState.Init -> Unit
        }
    }

    private fun setupAdapter(list: List<DayEntity>) {
        binding.daysRw.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = DaysAdapter(list)
            setHasFixedSize(true)
        }
    }

}