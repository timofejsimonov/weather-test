package com.example.weathertest.ui.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import com.example.weathertest.R
import com.example.weathertest.databinding.ItemDayBinding
import com.example.weathertest.domain.entity.DayEntity

class DaysAdapter(private val items: List<DayEntity>) :
    RecyclerView.Adapter<DaysAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        ItemDayBinding.inflate(LayoutInflater.from(parent.context), parent, false),
        Glide.with(parent.context)
    )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount() = items.size

    class ViewHolder(
        private val binding: ItemDayBinding,
        private val glide: RequestManager
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(day: DayEntity) {
            binding.apply {
                glide.load("https:${day.conditionIcon}").into(imageView)
                date.text = day.date
                temperature.text = day.avgTempC.toString()
                text.text = day.conditionText
                wind.text = imageView.context.getString(R.string.wind, day.maxWindKph.toString())
                humidity.text = imageView.context.getString(R.string.humidity, day.avgHumidity.toString())
            }
        }
    }
}